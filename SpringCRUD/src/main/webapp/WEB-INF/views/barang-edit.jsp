<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html ; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div align="center">
	<h1>EDIT BARANG</h1>
	<form:form action="${pageContext.request.contextPath}/barang-update" method="post" modelAttribute="barang" >
	${Sukses}
	<table>

		<tr>
			<td> ID Barang :</td>
			<td><form:input path="idBarang" readonly="true"/></td>		
		</tr>

		<tr>
			<td> Nama Barang :</td>
			<td><form:input path="namaBarang" required="required"/></td>	
			</tr>	
	
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Update"><a href="${contextPath}/barang-list">Kembali</a></td>
		</tr>
		
		
	</table>
	
	</form:form>

</div>
</body>
</html>