<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">

		<h1>New Person</h1>
		<form:form action="saveOrang" method="post">
			<table>
				<tr>
					<td>ID Orang :</td>
					<td><input type="number" name="idOrang" required="required" /></td>
				</tr>

				<tr>
					<td>Nama Orang :</td>
					<td><input type="text" name="namaOrang" required="required" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>

	</div>
</body>
</html>