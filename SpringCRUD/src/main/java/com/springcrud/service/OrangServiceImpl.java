package com.springcrud.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcrud.dao.OrangDao;
import com.springcrud.model.Orang;

@Transactional
@Service
public class OrangServiceImpl implements OrangService{
	@Autowired
	OrangDao orangDao;

	public List<Orang> getAll() {
		// TODO Auto-generated method stub

		List<Orang> orangs = null;
		try {

			orangs = new ArrayList<Orang>();
			orangs = orangDao.getAll();

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

		return orangs;
	}

	public Orang getById(Long id) {
		// TODO Auto-generated method stub
		Orang orang = null;
		
		try {
			orang = orangDao.getById(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return orang;
	}

	public void delete(Long id) {
		// TODO Auto-generated method stub
		Orang orang= null;
		try {
			orang = new Orang();
			orang = this.getById(id);
			if(orang !=null)
			{
				orangDao.delete(orang);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}
		

	}

	public void update(Orang entity) {
		// TODO Auto-generated method stub
		try {
			if(entity.getNamaOrang()!=null)
			{
				orangDao.update(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

	}

	public void save(Orang entity) {
		// TODO Auto-generated method stub

		try {
			
			
			if (entity.getNamaOrang() != null) {
				System.out.println("MASUK SAVE SERVICE");
				orangDao.save(entity);
				System.out.println("SUDAH SAVE SERVICE");

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

	}
}
