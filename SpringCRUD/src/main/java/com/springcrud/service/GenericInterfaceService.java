package com.springcrud.service;

import java.util.List;

public interface GenericInterfaceService<T, T2> {
	public List<T> getAll ();
	public T getById(T2 id);
	public void delete(T2 entity);
	public void update(T entity);
	public void save(T entity);
}
