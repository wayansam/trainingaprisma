package com.springcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="Barang")
public class Barang {
	@Id
	private long idBarang;
	
	@Column
	private String namaBarang;
	
	@ManyToOne
	@JoinColumn(name="idOrang",referencedColumnName="idOrang",insertable=true,updatable=true)
	private Orang orangs;
	
	@Transient
	private long idOrang;

	public long getIdOrang() {
		return idOrang;
	}

	public void setIdOrang(long idOrang) {
		this.idOrang = idOrang;
	}

	public Orang getOrangs() {
		return orangs;
	}

	public void setOrangs(Orang orangs) {
		this.orangs = orangs;
	}

	public long getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	
}
