package com.springcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Orang")
public class Orang {

	@Id
	private long idOrang;
	
	@Column
	private String namaOrang;
	
	@OneToMany(mappedBy = "orangs", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private Set<Barang> bar;

	public Set<Barang> getBar() {
		return bar;
	}

	public void setBar(Set<Barang> bar) {
		this.bar = bar;
	}

	public long getIdOrang() {
		return idOrang;
	}

	public void setIdOrang(long idOrang) {
		this.idOrang = idOrang;
	}

	public String getNamaOrang() {
		return namaOrang;
	}

	public void setNamaOrang(String namaOrang) {
		this.namaOrang = namaOrang;
	}
}
