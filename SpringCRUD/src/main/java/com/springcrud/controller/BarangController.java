package com.springcrud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.springcrud.model.Barang;
import com.springcrud.model.Orang;
import com.springcrud.service.BarangService;
import com.springcrud.service.OrangService;

@Controller
public class BarangController {
	@Autowired
	BarangService barangService;
	
	@Autowired
    OrangService orangService;
	
	// Method Add Person
	@RequestMapping(value = "/addBarang")
	public ModelAndView addBarang(Model model) {
		ModelAndView modelAndView = new ModelAndView("barang-add");

		try {
			List<Orang> orangs = new ArrayList<Orang>();
			
			//mengambil semua data person
			orangs = orangService.getAll();
			
			//Memasukan data person ke dalam View
			modelAndView.addObject("orangs", orangs);
			
			Barang item = new Barang();
			
			modelAndView.addObject("item", item);

		} catch (Exception e) {
			System.err.println("Error Add Barang");
		}

		return modelAndView;

	}

	// Method Save Person
	@RequestMapping(value = "/saveBarang", method = RequestMethod.POST)
	public ModelAndView saving(@ModelAttribute Barang item) {
		ModelAndView modelAndView = new ModelAndView("barang-list");

		try {

			if (item != null) {

				Orang pers = new Orang();
				pers = orangService.getById(item.getIdOrang());
				item.setOrangs(pers);
				
				barangService.save(item);			

				modelAndView.addObject("item", item);

				System.err.println("after save");
			} else {
				modelAndView.addObject("message", "Data tidak boleh kosong");
			}

		} catch (Exception e) {
			// TODO: handle exception

			System.err.println("controller /saveItem: " + e);
		}

		return new ModelAndView("redirect:/barang-list");
	}

	// Method Menampilkan List Person
	@RequestMapping(value = "/barang-list")
	public ModelAndView getAllData() {

		ModelAndView modelAndView = null;
		List<Barang> item = null;
		try {
			item = new ArrayList<Barang>();
			modelAndView = new ModelAndView("barang-list");

			item = barangService.getAll();

			modelAndView.addObject("listBarang", item);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return modelAndView;
	}

	// Method Delete Person
	@RequestMapping(value = "/barang-delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteBarang(@PathVariable(value = "id") Long id) {

		try {
			
			barangService.delete(id);

		} catch (Exception e) {
			System.err.println("Error Delete Barang" + e);
		}

		return new ModelAndView("redirect:/barang-list");
	}

	// Method Edit Person
	@RequestMapping(value = "/barang-edit/{id}")
	public ModelAndView editBarang(@PathVariable(value = "id") Long id) {

		ModelAndView modelAndView = new ModelAndView("barang-edit");
		Barang item = null;
		try {

			item = new Barang();
			item = barangService.getById(id);
			modelAndView.addObject("barang", item);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("error reqMap controller barang-edit " + e);
		}
		
		return modelAndView;
	}

	
	//Method Update Person
	@RequestMapping(value = "/barang-update", method = RequestMethod.POST)
	public ModelAndView updateBarang(@ModelAttribute Barang item) {

		ModelAndView modelAndView = new ModelAndView("barang-edit");
		try {

			if (item != null) {
				barangService.update(item);
			}
			modelAndView.addObject("Sukses", "Data berikut berhasil di update");
			modelAndView.addObject("item", item);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("controller / updateBarang:  " + e);
		}

		return new ModelAndView("redirect:/barang-list");
	}
}
