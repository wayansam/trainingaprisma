package com.springcrud.dao;

import java.util.List;

public interface GenericInterfaceDao<T,T2> {
	public List<T> getAll ();
	public T getById(T2 id);
	public void delete(T entity);
	public void update(T entity);
	public void save(T entity);
}
