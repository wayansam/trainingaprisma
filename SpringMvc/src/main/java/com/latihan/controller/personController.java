package com.latihan.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller //penting
public class personController {

//	@GetMapping
//	public String helloworld (Model model) {
//		model.addAttribute("my name using Model to send param","hel");
//		return "helloworld";
//	}

	@GetMapping("/hello2")
	public String passParametersWithModel(Model model) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("spring","mvc");
		model.addAttribute("pesan","Model");
		model.mergeAttributes(map);
		return "hello2";
	}
	
	
	@RequestMapping(value="/hello")
	public String passParametersWithModelMap(ModelMap map) {
		map.addAttribute("welcomeMessage","welcome");
		map.addAttribute("message","Baeldung");
		return "hello"; //nama filenya	
	}
	

	@GetMapping("/hello3")
	public ModelAndView passParametersWithModelAndView() {
	    ModelAndView modelAndView = new ModelAndView("hello");
	    modelAndView.addObject("message", "huehuehuemau");
	    return modelAndView;
	}
	

	@RequestMapping(value="/helloworld")
	public ModelAndView update (ModelMap map, Model model) {
		model.addAttribute("hel", "Model");
		map.put("hell", "Model Map");
		ModelAndView modelAndView = new ModelAndView("hello");
		return modelAndView;
	}
}
