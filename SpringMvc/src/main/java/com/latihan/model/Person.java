package com.latihan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person {
	
	@Id
	private long NIK;
	
	@Column
	private String Name;

	public long getNIK() {
		return NIK;
	}

	public void setNIK(long nIK) {
		NIK = nIK;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	
	

}
