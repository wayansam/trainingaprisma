package com.example.demo.controller;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.service.AttendanceService;
import com.example.demo.service.EmployeesService;
import com.example.demo.service.PositionsService;

@RestController
public class MainController {

	// ==================================== Employee
	// ===================================
	
	@Autowired
	private EmployeesService es;

	// http://localhost:8080/getempid?empId=5
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		return es.findByEmpID(empId);
	}

	// http://localhost:8080/getallposid?posId=1
	@GetMapping("/getallposid")
	public List<Employees> getEmpAllbyPosId(@RequestParam Integer posId) {
		return (List<Employees>) es.findByPosID(posId);
	}

	// http://localhost:8080/getempname?empName=Sultan
	@GetMapping("/getempname")
	public List<Employees> getEmpByEmpName(@RequestParam String empName) {
		return es.findByEmpName(empName);
	}

	// http://localhost:8080/getempgender?gender=M
	@GetMapping("/getempgender")
	public List<Employees> getEmpByGender(@RequestParam String gender) {
		return es.findByGender(gender);
	}

	// http://localhost:8080/getempage?age=19
	@GetMapping("/getempage")
	public List<Employees> getEmpByAge(@RequestParam Integer age) {
		return es.findByAge(age);
	}

	// http://localhost:8080/getempagegender?age=19&gender=M
	@GetMapping("/getempagegender")
	public List<Employees> getEmpByAgeGender(@RequestParam Integer age, @RequestParam String gender) {
		return es.findByAgeAndGender(age, gender);
	}

	// http://localhost:8080/getempall
	@GetMapping("/getempall")
	public List<Employees> getEmpAll() {
		return (List<Employees>) es.findAll();
	}

	/*
	 * http://localhost:8080/setemp/ raw ->JSON add
	 * {"empName":"Fiqa","posID":1,"gender":"F","age":21} update
	 * {"empID":5,"empName":"Sultan","posID":1,"gender":"M","age":19}
	 */
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		return es.setEmp(e);
	}

	/*
	 * http://localhost:8080/setemp/ raw ->JSON delete
	 * {"empID":5,"empName":"Sultan","posID":1,"gender":"M","age":19}
	 */
	@PostMapping("/delemp")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		return es.deleteEmp(e);
	}

	// ======================================= Position
	// =================================

	@Autowired
	private PositionsService ps;

	// http://localhost:8080/getposid?posId=1
	@GetMapping("/getposid")
	public Position getByPosId(@RequestParam Integer posId) {
		return ps.findByPosID(posId);
	}

	// http://localhost:8080/getposid?posname=Business Analys
	@GetMapping("/getposname")
	public Position getByPosName(@RequestParam String posName) {
		return ps.findByPosName(posName);
	}

	// http://localhost:8080/getposallowance?allowance=10000000
	@GetMapping("/getposallowance")
	public List<Position> getByAllowance(@RequestParam Integer allowance) {
		return ps.findByAllowance(allowance);
	}

	// http://localhost:8080/getposall
	@GetMapping("/getposall")
	public List<Position> getPosAll() {
		return (List<Position>) ps.findAll();
	}

	/*
	 * http://localhost:8080/setpos/ raw ->JSON add
	 * {"posName":"IT Analys","allowance":25000000} update
	 * {"posID":1,"posName":"Business Analys","allowance":5000000}
	 */
	@PostMapping("/setpos")
	public Map<String, String> setPos(@RequestBody Position e) {
		return ps.setPos(e);
	}

	/*
	 * http://localhost:8080/delpos/ raw ->JSON delete
	 * {"empID":5,"empName":"Sultan","posID":1,"gender":"M","age":19}
	 */
	@PostMapping("/delpos")
	public Map<String, String> delPos(@RequestBody Position e) {
		return ps.delPos(e);
	}

	// ========================================= Attendance
	// ================================

	@Autowired
	private AttendanceService as;

	// http://localhost:8080/getattid?attenId=1
	@GetMapping("/getattid")
	public Attendance getByAttenId(@RequestParam Integer attenId) {
		return as.findByAttenId(attenId);
	}

	// http://localhost:8080/getattall
	@GetMapping("/getattall")
	public List<Attendance> getAttAll() {
		return (List<Attendance>) as.findAll();
	}

	// http://localhost:8080/getallattempid?empID=1
	@GetMapping("/getallattempid")
	public List<Attendance> getAllAttbyEmpID(@RequestParam Integer empID) {
		return (List<Attendance>) as.findByEmpID(empID);
	}

	// http://localhost:8080/getallattstatus?attStatus=Present
	@GetMapping("/getallattstatus")
	public List<Attendance> getAllAttbyAttStatus(@RequestParam String attStatus) {
		return (List<Attendance>) as.findByAttStatus(attStatus);
	}

	// http://localhost:8080/getallattdate?attDate=2019-05-06
	@GetMapping("/getallattdate")
	public List<Attendance> getAllAttbyAttDate(@RequestParam Date attDate) {
		return (List<Attendance>) as.findByAttDate(attDate);
	}

	/*
	 * http://localhost:8080/setatt/ raw ->JSON add
	 * {"empID":1,"attDate":"2019-05-06","attStatus":"Present"} update
	 * {"attenId":4,"empID":1,"attDate":"2019-05-06","attStatus":"Present"}
	 */
	@PostMapping("/setatt")
	public Map<String, String> setAtt(@RequestBody Attendance e) {
		return as.setAtt(e);
	}

	/*
	 * http://localhost:8080/delatt/ raw ->JSON delete
	 * {"attenId":4,"empID":1,"attDate":"2019-05-06","attStatus":"Present"}
	 */
	@PostMapping("/delatt")
	public Map<String, String> delAtt(@RequestBody Attendance e) {
		return as.delAtt(e);
	}
		
}
