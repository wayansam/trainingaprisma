package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Employees;

public interface EmployeesService {
	
	public Employees findByEmpID(Integer empID);
	public List<Employees> findByPosID(Integer posId);
	public List<Employees> findByEmpName(String empName);
	public List<Employees> findByGender(String gender);
	public List<Employees> findByAge(Integer age);
	public List<Employees> findAll();
	public Map<String, String> setEmp(Employees e);
	public Map<String, String> deleteEmp(Employees e);
	
	//2
	public List<Employees> findByAgeAndGender(Integer age, String gender);
}
