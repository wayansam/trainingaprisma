package com.example.demo.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.example.demo.model.Attendance;

public interface AttendanceService {
	
	public Attendance findByAttenId(Integer attendId);
	public List<Attendance> findByEmpID(Integer empId);
	public List<Attendance> findByAttStatus(String attStatus);
	public List<Attendance> findByAttDate(Date attDate);
	public List<Attendance> findAll();
	public Map<String, String> setAtt(Attendance e);
	public Map<String, String> delAtt(Attendance e);

}
