package com.example.demo.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Attendance;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.service.AttendanceService;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService{
	@Autowired
	private AttendanceRepo ar;

	@Override
	public Attendance findByAttenId(Integer attendId) {
		Attendance pos = new Attendance();
		pos = ar.findByAttenId(attendId);
		return pos;
	}

	@Override
	public List<Attendance> findByEmpID(Integer empId) {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findByEmpID(empId);
		return atts;
	}

	@Override
	public List<Attendance> findByAttStatus(String attStatus) {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findByAttStatus(attStatus);
		return atts;
	}

	@Override
	public List<Attendance> findByAttDate(Date attDate) {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findByAttDate(attDate);
		return atts;
	}

	@Override
	public List<Attendance> findAll() {
		List<Attendance> atts = new ArrayList<Attendance>();
		atts = (List<Attendance>) ar.findAll();
		return atts;
	}

	@Override
	public Map<String, String> setAtt(Attendance e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = ar.save(e);
			respMap.put("attid", e.getAttenId().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("attid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> delAtt(Attendance e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			ar.delete(e);
			respMap.put("attid", e.getAttenId().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("attid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

}
