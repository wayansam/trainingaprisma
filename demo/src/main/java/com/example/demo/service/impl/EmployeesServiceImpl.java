package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.service.EmployeesService;

@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesService{
	
	@Autowired
	private EmployeesRepo er;

	@Override
	public Employees findByEmpID(Integer empID) {
		// TODO Auto-generated method stub
		Employees employees = new Employees();
		employees = er.findByEmpID(empID);
		return employees;
	}

	@Override
	public List<Employees> findByPosID(Integer posId) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posId);
		return empls;
	}

	@Override
	public List<Employees> findByEmpName(String empName) {
		// TODO Auto-generated method stub
		List<Employees> employees = new ArrayList<Employees>();
		employees = er.findByEmpName(empName);
		return employees;
	}

	@Override
	public List<Employees> findByGender(String gender) {
		// TODO Auto-generated method stub
		List<Employees> employees = new ArrayList<Employees>();
		employees = er.findByGender(gender);
		return employees;
	}

	@Override
	public List<Employees> findByAge(Integer age) {
		// TODO Auto-generated method stub
		List<Employees> employees = new ArrayList<Employees>();
		employees = er.findByAge(age);
		return employees;
	}

	@Override
	public List<Employees> findAll() {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}

	@Override
	public Map<String, String> setEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> deleteEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

	@Override
	public List<Employees> findByAgeAndGender(Integer age, String gender) {
		// TODO Auto-generated method stub
		List<Employees> employees = new ArrayList<Employees>();
		employees = er.findByAgeAndGender(age, gender);
		return employees;
	}

}
