package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Position;
import com.example.demo.repository.PositionsRepo;
import com.example.demo.service.PositionsService;

@Service
@Transactional
public class PositionsServiceImpl implements PositionsService{
	
	@Autowired
	private PositionsRepo pr;
	
	@Override
	public Position findByPosID(Integer posId) {
		Position pos = new Position();
		pos = pr.findByPosID(posId);
		return pos;
	}

	@Override
	public Position findByPosName(String posName) {
		Position pos = new Position();
		pos = pr.findByPosName(posName);
		return pos;
	}

	@Override
	public List<Position> findByAllowance(Integer allowance) {
		List<Position> pos = new ArrayList<Position>();
		pos = pr.findByAllowance(allowance);
		return pos;
	}

	@Override
	public List<Position> findAll() {
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findAll();
		return pos;
	}

	@Override
	public Map<String, String> setPos(Position e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = pr.save(e);
			respMap.put("posid", e.getPosID().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("posid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

	@Override
	public Map<String, String> delPos(Position e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			pr.delete(e);
			respMap.put("posid", e.getPosID().toString());
			respMap.put("code", HttpStatus.OK + "");
			respMap.put("msg", "success");

		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("posid", "");
			respMap.put("code", HttpStatus.CONFLICT + "");
			respMap.put("msg", "failed");
		}
		return respMap;
	}

}
