package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Position;

public interface PositionsService {

	public Position findByPosID(Integer posId);
	public Position findByPosName(String posName);
	public List<Position> findByAllowance(Integer allowance);
	public List<Position> findAll();
	public Map<String, String> setPos(Position e);
	public Map<String, String> delPos(Position e);
	
}
