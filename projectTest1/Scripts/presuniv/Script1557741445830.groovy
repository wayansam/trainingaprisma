import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://presuniv.net/')

WebUI.click(findTestObject('Object Repository/presuniv/Page_President University e-Campus/a_Log in'))

WebUI.setText(findTestObject('Object Repository/presuniv/Page_President University e-Campus/input_Username_username'), '001201600029')

WebUI.setEncryptedText(findTestObject('Object Repository/presuniv/Page_President University e-Campus/input_Password_password'), 
    '8aV4N2ijgN9ObFRXqNJ0rA==')

WebUI.click(findTestObject('Object Repository/presuniv/Page_President University e-Campus/input_Password_btn btn-primary btn-block'))

WebUI.click(findTestObject('Object Repository/presuniv/Page_Dashboard/a_Log out'))

